import os.path
import yaml


class Config(object):

    filename = None
    settings = {}
    def __init__(self, filename='config.yaml', edit=False):
        self.filename = filename

        # file exists
        if not os.path.isfile(self.filename) or edit:
            self.create_config()
        else:
            with open(self.filename, 'r') as f:
                self.settings = yaml.load(f)
                print('Settings loaded.')

        print(self.settings)

    def create_config(self):
        options = {
            'quandl-api': 'Your API key in Quandl: ',
            'quandl-full-zip': 'Filename in which download prices from Quandl (quandl-full.zip): ',
            'psql-host': 'PostgreSQL host (localhost): ',
            'psql-user': 'PostgreSQL user (user): ',
            'psql-password': 'PostgreSQL password (password): ',
            'psql-db': 'PostgreSQL database name (db): ',
        }
        print('Fill settings, please.')
        for n, desc in options.items():
            self.settings[n] = input(desc)

        if not self.settings['quandl-full-zip']:
            self.settings['quandl-full-zip'] = 'quandl-full.zip'

        if not self.settings['psql-host']:
            self.settings['psql-host'] = 'localhost'

        if not self.settings['psql-user']:
            self.settings['psql-user'] = 'user'

        if not self.settings['psql-password']:
            self.settings['psql-password'] = 'password'

        if not self.settings['psql-db']:
            self.settings['psql-db'] = 'db'

        with open(self.filename, 'w') as f:
            yaml.dump(self.settings, f)
            print('Settings saved in {0}.'.format(self.filename))
